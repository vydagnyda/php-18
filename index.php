<?php

require_once 'Dog.php';
require_once 'Poodle.php';

$dog = new Dog('test');
$poodle = new Poodle('test2');

var_dump($dog);
var_dump($poodle);

var_dump(get_class($dog));
var_dump(get_class($poodle));

var_dump($dog instanceof Dog); //ar dog yra objectas klases Dog??
var_dump($dog instanceof Poodle); 

var_dump($poodle instanceof Dog); //ar poodle yra objectas klases Dog??
var_dump($poodle instanceof Poodle); 

$puppy = new Poodle('Sargis'); // konstruktorius (iš tėviškos klasės)
$puppy->set_type(12); // metodas iš dukterinės klasės
echo "Poodle is called ". $puppy->name; //kintamasis iš tėviškos klasės
echo "of type ". $puppy->type; //kintamasis iš dukterinės klasės
echo $puppy->bark(); //metodas iš tėviškos klasės
