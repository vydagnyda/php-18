<?php
require_once 'Device.php';

final class Memory extends Device 
{
    protected $type;
    protected $amount;
    protected $speed;

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    public function getCard()
    {
        return 'Memory card';
    }

    public function __toString()
    {
        return 'Memory drive';
    }
}