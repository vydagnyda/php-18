<?php
require_once 'Device.php';

class HardDrive extends Device 
{
    protected $amount;

    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCard()
    {
        return 'card';
    }

    public function __toString()
    {
        return 'Hard drive';
    }
}