<?php
abstract class Device //klase padarom abstrakcia ir neleis kurti jokiu objektu tos klases
{
    protected $serialNumber; 
    protected $manufacturer;
    protected $model;
    protected $sku;

    public function __construct($serialNumber, $sku)
    {
        $this->serialNumber = $serialNumber;
        $this->sku = $sku;
    }

    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }
      

    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }


    abstract public function getInventoryDetails();
    
}


